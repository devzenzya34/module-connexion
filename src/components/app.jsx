import React, { Component } from 'react'
import '../styles/style.css';
import Header from "../containers/header";
import { Route, Switch } from "react-router-dom"
import Home from "./home";
import TodoApp from "./todo-app";
import Signin from "../containers/signin"
import Signout from "../containers/signout"
import Signup from "../containers/signup"
import Ressources from "./ressources";
import RequireAuthentification from '../helpers/require-authentification'
import Errors from "./errors";

class App extends Component {
    render() {
    return (
        <div>
            <Header />
            <div className="container body-content">
                <Errors />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/ressources" component={RequireAuthentification(Ressources)} />
                    <Route exact path="/signin" component={Signin} />
                    <Route exact path="/signout" component={Signout} />
                    <Route exact path="/signup" component={Signup} />
                    <Route exact path="/todoapp" component={RequireAuthentification(TodoApp)} />
                </Switch>
            </div>
        </div>
        );
    }
}

export default App;
