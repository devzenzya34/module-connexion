import React, { Component } from "react";
import { addRessource, getSpecialRessource } from "../actions"
import { connect } from "react-redux";
import { getRessourceList, getContainsOneRessourceList, getPrimeNumberRessourceList, getSpecialNumber } from "../selectors"

class Ressources extends Component {

    componentWillMount() {
        this.props.getSpecialRessource()
    }

    renderRessources = (ressourceList) => {
        return ressourceList.map((ressource, index) => <li key={index}>{ressource}</li>)
    }

    render() {
        console.log(this.props.ressourceList)
        return (
            <div className="container">
                <div className="mt-5">
                    <h4>{this.props.message}</h4>
                </div>
                <div className="mt-5 row">
                    <div className="col">
                        <button type="button" onClick={() => this.props.addRessource()} className="btn btn-raised btn-success">Ajouter un nombre</button>
                    </div>
                    <div className="col">
                        <span>Entiers</span>
                        <ul>
                            {this.renderRessources(this.props.ressourceList)}
                        </ul>
                    </div>
                    <div className="col">
                        Contiennent "1"
                        <ul>
                            {this.renderRessources(this.props.containsOneRessourceList)}
                        </ul>
                    </div>
                    <div className="col">
                        Entiers premiers
                        <ul>
                            {this.renderRessources(this.props.primeRessourceList)}
                        </ul>
                    </div>
                    <div className="col">
                        Entiers premiers contenants "1"
                        <ul>
                            {this.renderRessources(this.props.specialNumber)}
                        </ul>
                    </div>
                </div >
            </div>
        )
    }
}

const mapStateToProps = state => ({
    ressourceList: getRessourceList(state),
    containsOneRessourceList: getContainsOneRessourceList(state),
    primeRessourceList: getPrimeNumberRessourceList(state),
    specialNumber: getSpecialNumber(state),
    message: state.ressource.message
});

const mapDispatchToProps = {
    addRessource,
    getSpecialRessource
};

export default connect(mapStateToProps, mapDispatchToProps)(Ressources);