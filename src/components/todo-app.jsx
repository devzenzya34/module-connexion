import React, { Component } from 'react'


export default class TodoApp extends Component {
    state = { todoList: [], input: "" }

    onButtonPress = (e) => {
        const input = this.state.input;
        this.setState({ todoList: [...this.state.todoList, input], input: "" });
    }

    renderTodoList = () => {
        return this.state.todoList.map((todo, index) => {
            return (<div key={index} className="list-group-item list-group-item-secondary list-group-item-action flex-column align-items-start">
                {todo}
            </div>)
        })

    }
    render() {

        return (
            <div className="container">
                <div className="row">
                    <div className="mt-5 col-md-12 justify-content-center">
                        <fieldset className="form-group">
                            <label htmlFor="inputTodo" className="bmd-label-floating">Nouvelle tâche</label>
                            <input id="inputTodo" onChange={(e) => this.setState({ input: e.target.value })} value={this.state.input} className="form-control" />
                        </fieldset>
                        <div className="float-right mb-5">
                            <button id="addButton" className="btn btn-primary btn-raised" onClick={this.onButtonPress}>Ajouter</button>
                        </div>
                    </div>
                </div>
                <div className="row">
                    {this.renderTodoList()}
                </div>
            </div >
        );
    }
}