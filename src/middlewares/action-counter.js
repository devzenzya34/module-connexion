import { incrementActionCount } from "../actions";
import {INCREMENT_ACTION_COUNT} from "../actions/actions-types";

export const actionCounter = (store) => {
    return (next) => {
        return (action) => {
            if(action.type !== INCREMENT_ACTION_COUNT) {
                store.dispatch(incrementActionCount())
            }
            //console.log("test middleware", action)
            next(action)
        }
    }
}