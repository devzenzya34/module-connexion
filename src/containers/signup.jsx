import React, {Component} from 'react';
import { Field, reduxForm } from "redux-form"
import * as actions from "../actions"
import * as validations from "../validations";
import { connect } from "react-redux"

const FIELDS = { email: "email", password: "password", secondPassword: "secondPassword"}

class Signup extends Component {

    myHandleSubmit = formValues => {
        this.props.signupUser(formValues, this.props.history)
    }

    renderField = (field) => {
        return (
            <div className="row justify-content-md-center">
                <fieldset className="col-md-4 form-group">
                    <label className="bmd-label-floating">{field.label}</label>
                    <input {...field.input} type={field.type} className="form-control" />
                    {field.meta.touched && field.meta.error && <span className="error">{field.meta.error}</span>}
                </fieldset>
            </div>
        )
    }

    render() {

        return (
            <form onSubmit={this.props.handleSubmit(this.myHandleSubmit)}>
                <div className="mt-5 row justify-content-md-center">
                    <h1>Inscription</h1>
                </div>
                <Field
                    name={FIELDS.email}
                    component={this.renderField}
                    type='text'
                    label='Email'

                />
                <Field
                    name={FIELDS.password}
                    component={this.renderField}
                    type='password'
                    label='Mot de passe'
                />
                <Field
                    name={FIELDS.secondPassword}
                    component={this.renderField}
                    type='password'
                    label='Confirmez Mot de passe'
                />

                <div className="row justify-content-md-center">
                    <button type="submit" className="btn btn-success btn-raised">Inscription</button>
                </div>
            </form>
        )
    }
}

//traitement du formulaire
function validate(formValues) {
    const errors = {};
    errors.email = validations.validateEmail(formValues.email);
    errors.password = validations.validateNotEmpty(formValues.password)
    errors.secondPassword = validations.validateEqual(formValues.password, formValues.secondPassword)
    return errors;
}
const SignupForm = reduxForm({
    form: 'SignupForm',
    fields: Object.keys(FIELDS),
    validate
})(Signup);


export default connect(null, actions)(SignupForm)