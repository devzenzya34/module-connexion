import React, {Component} from 'react';
import { connect } from 'react-redux'
import * as actions from '../actions'
import { Link } from "react-router-dom";

class Header extends Component {

    /*onClickAuthentification = () => {
        this.props.setAuthentification(!this.props.isLoggedIn)
    }*/

    //Gestion du changement de label selon l'état
    renderAuthentificationLabel = () => {
        if(this.props.isLoggedIn) {
            return "Déconnexion"
        } else {
            return "Connexion"
        }
    }

    //function qui gère le lien de connexion/deconnexion du header
    renderAuthentificationLink = () => {
        if(this.props.isLoggedIn) {
            return (
                <>
                <Link to="/signout" className="nav-link">{this.renderAuthentificationLabel()}</Link>
                <Link to="/ressources" className="nav-link">Ressources</Link>
                <Link to="/todoapp" className="nav-link">TodoApp</Link>
                </>
            )
        } else {
            return (
                <>
                    <Link to="/signin" className="nav-link">{this.renderAuthentificationLabel()} |</Link>
                    <Link to="/signup" className="nav-link">Inscription</Link>
                </>

            )
        }
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container">
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav">
                            <Link to="/" className="nav-link active">Accueil<span className="sr-only">(current)</span></Link>
                            {this.renderAuthentificationLink()}
                        </div>
                    </div>
                </div>
            </nav>
        );
    }
}

const mapStateToProps = (state) => {
    return {
    isLoggedIn: state.authentification.isLoggedIn
    }
}

export default connect(mapStateToProps, actions)(Header);
export { Header }