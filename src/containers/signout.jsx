import React, {Component} from 'react';
import * as actions from "../actions"
import { connect } from "react-redux"

class Signout extends Component {
    componentWillMount() {
        this.props.signoutUser()
    }

    render() {
        return (
            <div className="mt-5 container">
                <h1>Merci de votre visite. Aurevoir et à bientôt!</h1>
            </div>
        );
    }
}

export default connect(null, actions)(Signout);




