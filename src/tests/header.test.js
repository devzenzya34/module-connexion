import React from "react";
import Header from "../containers/header";
import { shallow, mount } from "enzyme";
import { setAuthentification, incrementActionCount } from "../actions";
import AuthentificationReducer from "../reducers/authentification";
import { SET_AUTHENTIFICATION, INCREMENT_ACTION_COUNT } from "../actions/actions-types";
import RootTest from "./root.test";

describe("Test sur le composant connecté Header", () => {

    it("Render du composant connecté sans erreur", () => {
        const wrapper = shallow(
            <RootTest>
                <Header />
            </RootTest>
        )
    })

    it("Test que le libellé du bouton de connexion change à chaque clique", () => {
        const wrapper = mount(
            <RootTest>
                <Header />
            </RootTest>
        )
        //test si l'objet existe
        expect(wrapper.find("a").at(3).text()).toEqual("Connexion")
        //test du click, simulation du click
        wrapper.find("a").at(3).simulate("click")
        console.log(wrapper.debug())
        //test si deconnexion ap click
        expect(wrapper.find("a").at(3).text()).toEqual("Déconnexion")

    })

    //test simple sur le contenu d'une action
    it("Test le payload d'une action", () => {
        const action = incrementActionCount()
        expect(action.type).toEqual(INCREMENT_ACTION_COUNT)
    })

    //test simple sur le reducer
    it("Test le reducer authentification sans action type", () => {
        const action = { type: SET_AUTHENTIFICATION, payload: true}
        const initialState = {
            isLoggedIn: false
        }
        //expect(AuthentificationReducer(initialState, {}).isLoggedIn).toEqual(false)
        expect(AuthentificationReducer(initialState, action).isLoggedIn).toEqual(true)
    })
})