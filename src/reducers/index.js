import { combineReducers } from 'redux'
import AuthentificationReducer from "./authentification";
import ActionCountReducer from "./action-count"
import RessourceReducer from "./ressources";
import { reducer as form } from "redux-form"
import ErrorReducer from "./errors";

const rootReducer = combineReducers({
    authentification: AuthentificationReducer,
    actionCount: ActionCountReducer,
    ressource: RessourceReducer,
    form: form,
    errors: ErrorReducer
});

export default rootReducer;