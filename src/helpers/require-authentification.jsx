import React, {Component} from 'react';
import {connect} from 'react-redux'

export default function (ChildComponent) {

    class RequireAuthentification extends Component {

        //Bloque l'accès à la page des ressources si on n'est pas loggé et renvoi à la page d'accueil
        componentWillMount() {
            if(!this.props.isLoggedIn) {
                this.props.history.push("/")
            }
        }
        componentWillUpdate(nextProps) {
            if(!nextProps.isLoggedIn) {
                this.props.history.push("/")
            }
        }

        render() {
            return this.props.isLoggedIn && <ChildComponent />
        }
    }

    const mapStateToProps = (state) => ({
        isLoggedIn: state.authentification.isLoggedIn
    })

    return connect(mapStateToProps)(RequireAuthentification);
}

