import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux"
import { createStore, applyMiddleware } from "redux"
import thunk from "redux-thunk";
import './styles/style.css';
import App from './components/app';
import reportWebVitals from './reportWebVitals';
import reducers from "./reducers"
import { actionCounter } from "./middlewares/action-counter"
import { BrowserRouter } from "react-router-dom";
import { setAuthentification } from "./actions";


const createStoreWithMiddleware = applyMiddleware(thunk, actionCounter)(createStore);

const store = createStoreWithMiddleware(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

//recupération du token avt tt le render de l'app
const token = localStorage.getItem("token")
if(token) {
    store.dispatch(setAuthentification(true))
}

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
