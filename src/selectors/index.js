import { createSelector } from "reselect";
import lodash from 'lodash'

export const getRessourceList = state => {
    return state.ressource.ressourceList
}

export const getContainsOneRessourceList = state => {
    //return state.ressource.ressourceList.filter(ressource => ressource.toString().indexOf('1') > -1)
    return getRessourceList(state).filter(ressource => ressource.toString().indexOf('1') > -1)
}

export const getPrimeNumberRessourceList = state => {
    return getRessourceList(state).filter (ressource => isPrimeNumber(ressource))
}
//funciton qui verifie si la veleur est un nombre premier
function isPrimeNumber(value){
    for ( var i = 2; i < value; i++) {
        if(value % i === 0){
            return false
        }
    }
    return value > 1;
}

//Combo de selecteur avec reselect
export const getSpecialNumber = createSelector(
    getContainsOneRessourceList,
    getPrimeNumberRessourceList,
    (containsOneRessourceList, primeNumberRessourceList) => {
        return lodash.intersection(containsOneRessourceList, primeNumberRessourceList)
    }
)

