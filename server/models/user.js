const mongoose = require ('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt-nodejs')

//creation du model
const userSchema = new Schema({
    email: {type:String, unique:true, lowercase:true},
    password:String
})

//sécurisation des données avt la fonction save de la creation du user
userSchema.pre('save', function(next) {
    const user = this
    bcrypt.genSalt(10, function(err, salt) {
        if(err) {
            return next(err)
        }
        bcrypt.hash(user.password, salt, null, function(err, hash){
            if(err){
                return next(err)
            }
            user.password = hash
            next()
        })
    })
})

//verification methods User
userSchema.methods.arePasswordEqual = function (externalPassword, done) {
    bcrypt.compare(externalPassword, this.password, function (err, isMatch) {
        if (err) {
            return done(err);
        }
        done(null, isMatch)
    })
}
//collection = table User
const UserModel = mongoose.model("user", userSchema)

module.exports = UserModel
