//console.log("Hello du server!")
const express = require('express')
const bodyParser = require('body-parser')
const morgan = require("morgan")
const expressServer = express()
const router = require ("./routes")
const http = require("http")
const mongoose = require('mongoose')
const cors = require("cors")

mongoose.connect("mongodb+srv://zenzya:Rasabotsy25@cluster0.krexe.mongodb.net/moduleconnexiondb?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true })
mongoose.connection
    .once('open', () => console.log("Base de donnée MongoDb connecté"))
    .on('error', error => console.log("Erreur de connexion à la Bdd Mongo"))

expressServer.use(morgan('combined'))
expressServer.use(bodyParser.json({type: '*/*'}))
//Ajout de cors (blaocage des requetes par le navigateur chrome et mozzilla
expressServer.use(cors())

const port = 3090
const server = http.createServer(expressServer)
//on envoi tt le serveur sur la route
router(expressServer)
server.listen(port)
console.log("Hello du server! Ecoute sur le port 3090")
