const User = require("../models/user")
const lodash = require("lodash")
const jwt = require("jwt-simple")
const config = require ("../../config.js")
const passport = require('passport')

//genere le token pour le user
function getTokenForUser(user) {
    const timeStamp = new Date().getTime()
    return jwt.encode({
        sub: user.id,
        iat: timeStamp
    },
    config.secret
    )
}

//controller pour l'inscription
exports.signup = function (req, res, next) {
    const email = req.body.email;
    const password = req.body.password;

    //instance pour verifier si l'email existe déja
    User.findOne({email: email}, function (err, existingUser) {
        if (err) {
            return next(err)
        }
        if (existingUser) {
            return res.status(422).send({error: "Email déja utilisé!"})
        }
        if (lodash.isEmpty(email) || lodash.isEmpty(password)) {
            return res.status(422).send({error: "Email ou mot de passe vide!"})
        } else {
            const user = new User({
                email: email,
                password: password
            })
            user.save(function(err){
                if(err){
                    return next(err)
                }
                res.json({ token: getTokenForUser(user) })
            })
        }
    })
}

//controller pour la connexion
exports.signin = function (req, res, next) {
    passport.authenticate("local", function(err, user, info) {
        if(err) {
            return next(err)
        }
        if(!user) {
            return res.status(500).send({message: "Les identifiants sont invalides"})
        } else {
            res.json({token: getTokenForUser(req.user)})
        }
    })(req, res, next)
}