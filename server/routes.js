const AuthentificationController = require ('./controllers/authentification')
require('./services/passport')
const passport = require('passport')

const requireToken = passport.authenticate("jwt", {session: false})

module.exports = function (expressServer) {
    /*expressServer.get("/", function(req, res, next) {
        res.send({serverData: ["vide"]})
    })*/

    //route d'inscription
    expressServer.post("/signup", AuthentificationController.signup)

    //route de requete du token sécurisé
    expressServer.get("/specialressource", requireToken, function(req, res) {
        res.send({ result: "Ceci est un contenu sécurisé"})
    })

    //route de connexion
    expressServer.post("/signin", AuthentificationController.signin)
}