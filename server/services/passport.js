const passport = require('passport')
const User = require("../models/user")
const config = require("../../config")
const JwtStrategy = require("passport-jwt").Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const LocalStrategy = require("passport-local")

//recup les infos transmis dans le header de la requete
const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: config.secret
}

//décryptage des info et recherche du User
const jwtLogin = new JwtStrategy(jwtOptions, function(payload, done){
    const userId = payload.sub
    User.findById(userId, function(err, user) {
        if(err) {
            return done(err, false)
        }
        if(user) {
            return done(null, user)
        } else {
            return done(null, false)
        }
    })
})

//Strategy de connexion
//redifinir le mapping car il cherche username.field par défaut
const localOptions = { usernameField: "email"}
const localLoginStrategy = new LocalStrategy(localOptions, function(email, password, done) {
    User.findOne({email}, function(err, user){
        if(err) return done(err)
        if(!user) return done(null, false)
        user.arePasswordEqual(password, function(err, isMatch) {
            if(err) return done(err)
            if(!isMatch) return done(null, false)
            return done(null, user)
        })
    })
})

passport.use(jwtLogin)
passport.use(localLoginStrategy)

